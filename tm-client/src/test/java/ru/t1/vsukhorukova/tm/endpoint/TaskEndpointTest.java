package ru.t1.vsukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.dto.request.project.ProjectClearRequest;
import ru.t1.sukhorukova.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.sukhorukova.tm.dto.request.task.*;
import ru.t1.sukhorukova.tm.dto.request.user.UserLoginRequest;
import ru.t1.sukhorukova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sukhorukova.tm.dto.response.task.*;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.service.PropertyService;
import ru.t1.vsukhorukova.tm.marker.IntegrationCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.PROJECT1_DESC;
import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.PROJECT1_NAME;
import static ru.t1.vsukhorukova.tm.constant.TaskTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.USER_PASSWORD;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String getToken(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest(null);
        request.setLogin(login);
        request.setPassword(password);

        @Nullable final String token = authEndpoint.login(request).getToken();
        return token;
    }

    @Before
    public void login() {
        adminToken = getToken(ADMIN_LOGIN, ADMIN_PASSWORD);
        userToken = getToken(USER_LOGIN, USER_PASSWORD);
        taskEndpoint.clearTask(new TaskClearRequest(adminToken));
        taskEndpoint.clearTask(new TaskClearRequest(userToken));
        projectEndpoint.clearProject(new ProjectClearRequest(adminToken));
        projectEndpoint.clearProject(new ProjectClearRequest(userToken));
    }

    @After
    public void logout() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        userToken = null;
        adminToken = null;
    }

    @Test
    public void testCreateTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);

        @NotNull final TaskCreateResponse responseCreate = taskEndpoint.createTask(requestCreate);
        @Nullable Task taskCreate = responseCreate.getTask();
        Assert.assertNotNull(taskCreate);
        Assert.assertEquals(TASK1_NAME, taskCreate.getName());
        Assert.assertEquals(TASK1_DESC, taskCreate.getDescription());

        @NotNull final TaskListRequest requestListUser = new TaskListRequest(userToken);
        @NotNull final TaskListResponse responseListUser = taskEndpoint.listTask(requestListUser);
        @Nullable final List<Task> taskListUser = responseListUser.getTasks();
        Assert.assertNotNull(taskListUser);
        Assert.assertEquals(1, taskListUser.size());
        Assert.assertEquals(TASK1_NAME, taskListUser.get(0).getName());
        Assert.assertEquals(TASK1_DESC, taskListUser.get(0).getDescription());

        @NotNull final TaskListRequest requestListAdmin = new TaskListRequest(adminToken);
        @NotNull final TaskListResponse responseListAdmin = taskEndpoint.listTask(requestListAdmin);
        @Nullable final List<Task> taskListAdmin = responseListAdmin.getTasks();
        Assert.assertNull(taskListAdmin);

        @NotNull final TaskCreateRequest requestException = new TaskCreateRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.createTask(requestException);
    }

    @Test
    public void testListTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.createTask(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals(TASK1_NAME, taskList.get(0).getName());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK3_NAME, taskList.get(2).getName());
        Assert.assertEquals(TASK1_DESC, taskList.get(0).getDescription());
        Assert.assertEquals(TASK2_DESC, taskList.get(1).getDescription());
        Assert.assertEquals(TASK3_DESC, taskList.get(2).getDescription());
    }

    @Test
    public void testShowByIdTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreate).getTask().getId();

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.createTask(requestCreate);

        @NotNull final TaskShowByIdRequest requestShow = new TaskShowByIdRequest(userToken);
        requestShow.setTaskId(taskId);
        @NotNull final TaskShowByIdResponse responseShow = taskEndpoint.showByIdTask(requestShow);
        @Nullable final Task task = responseShow.getTask();
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(TASK2_DESC, task.getDescription());

        @NotNull final TaskShowByIdRequest requestException = new TaskShowByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.showByIdTask(requestException);
    }

    @Test
    public void testShowByIndexTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.createTask(requestCreate);

        @NotNull final TaskShowByIndexRequest requestShow = new TaskShowByIndexRequest(userToken);
        requestShow.setIndex(1);
        @NotNull final TaskShowByIndexResponse responseShow = taskEndpoint.showByIndexTask(requestShow);
        @Nullable final Task task = responseShow.getTask();
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(TASK2_DESC, task.getDescription());

        @NotNull final TaskShowByIndexRequest requestException = new TaskShowByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.showByIndexTask(requestException);
    }

    @Test
    public void testClearTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.createTask(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());

        @NotNull final TaskClearRequest requestClear = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(requestClear);

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNull(taskList);
    }

    @Test
    public void testRemoveByIdTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.createTask(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        @NotNull final String projectId = taskList.get(1).getId();

        @NotNull final TaskRemoveByIdRequest requestRemove = new TaskRemoveByIdRequest(userToken);
        requestRemove.setTaskId(projectId);
        taskEndpoint.removeByIdTask(requestRemove);

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertEquals(2, taskList.size());
        Assert.assertNotEquals(TASK2_NAME, taskList.get(1).getName());

        @NotNull final TaskRemoveByIdRequest requestException = new TaskRemoveByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.removeByIdTask(requestException);
    }

    @Test
    public void testRemoveByIndexTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK3_NAME);
        requestCreate.setDescription(TASK3_DESC);
        taskEndpoint.createTask(requestCreate);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(3, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        @NotNull final String taskId = taskList.get(1).getId();

        @NotNull final TaskRemoveByIndexRequest requestRemove = new TaskRemoveByIndexRequest(userToken);
        requestRemove.setIndex(1);
        taskEndpoint.removeByIndexTask(requestRemove);

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertEquals(2, taskList.size());
        Assert.assertNotEquals(TASK2_NAME, taskList.get(1).getName());

        @NotNull final TaskRemoveByIndexRequest requestException = new TaskRemoveByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.removeByIndexTask(requestException);
    }

    @Test
    public void testUpdateByIdTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK2_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIdRequest requestUpdate = new TaskUpdateByIdRequest(userToken);
        requestUpdate.setTaskId(taskId);
        requestUpdate.setName(TASK3_NAME);
        requestUpdate.setDescription(TASK3_DESC);
        @Nullable Task task = taskEndpoint.updateByIdTask(requestUpdate).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK3_NAME, task.getName());
        Assert.assertEquals(TASK3_DESC, task.getDescription());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK3_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK3_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIdRequest requestException = new TaskUpdateByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.updateByIdTask(requestException);
    }

    @Test
    public void testUpdateByIndexTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK2_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIndexRequest requestUpdate = new TaskUpdateByIndexRequest(userToken);
        requestUpdate.setIndex(1);
        requestUpdate.setName(TASK3_NAME);
        requestUpdate.setDescription(TASK3_DESC);
        @Nullable Task task = taskEndpoint.updateByIndexTask(requestUpdate).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK3_NAME, task.getName());
        Assert.assertEquals(TASK3_DESC, task.getDescription());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK3_NAME, taskList.get(1).getName());
        Assert.assertEquals(TASK3_DESC, taskList.get(1).getDescription());

        @NotNull final TaskUpdateByIndexRequest requestException = new TaskUpdateByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.updateByIndexTask(requestException);
    }

    @Test
    public void testChangeStatusByIdTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIdRequest requestChangeStatus = new TaskChangeStatusByIdRequest(userToken);
        requestChangeStatus.setTaskId(taskId);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable Task task = taskEndpoint.changeStatusByIdTask(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIdRequest requestException = new TaskChangeStatusByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.changeStatusByIdTask(requestException);
    }

    @Test
    public void testChangeStatusByIndexTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIndexRequest requestChangeStatus = new TaskChangeStatusByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable Task task = taskEndpoint.changeStatusByIndexTask(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskChangeStatusByIndexRequest requestException = new TaskChangeStatusByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.changeStatusByIndexTask(requestException);
    }

    @Test
    public void testCompleteByIdTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIdRequest requestChangeStatus = new TaskCompleteByIdRequest(userToken);
        requestChangeStatus.setTaskId(taskId);
        @Nullable Task task = taskEndpoint.completeByIdTask(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIdRequest requestException = new TaskCompleteByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.completeByIdTask(requestException);
    }

    @Test
    public void testCompleteByIndexTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIndexRequest requestChangeStatus = new TaskCompleteByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable Task task = taskEndpoint.completeByIndexTask(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, taskList.get(1).getStatus());

        @NotNull final TaskCompleteByIndexRequest requestException = new TaskCompleteByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.completeByIndexTask(requestException);
    }

    @Test
    public void testStartByIdTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskStartByIdRequest requestChangeStatus = new TaskStartByIdRequest(userToken);
        requestChangeStatus.setTaskId(taskId);
        @Nullable Task task = taskEndpoint.startByIdTask(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskStartByIdRequest requestException = new TaskStartByIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.startByIdTask(requestException);
    }

    @Test
    public void testStartByIndexTask() {
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName(TASK1_NAME);
        requestCreate.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreate);

        requestCreate.setName(TASK2_NAME);
        requestCreate.setDescription(TASK2_DESC);
        taskEndpoint.createTask(requestCreate).getTask().getId();

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, taskList.get(1).getStatus());

        @NotNull final TaskStartByIndexRequest requestChangeStatus = new TaskStartByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable Task task = taskEndpoint.startByIndexTask(requestChangeStatus).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());

        responseList = taskEndpoint.listTask(requestList);
        taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskList.get(1).getStatus());

        @NotNull final TaskStartByIndexRequest requestException = new TaskStartByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        taskEndpoint.startByIndexTask(requestException);
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final TaskCreateRequest requestCreateTask = new TaskCreateRequest(userToken);
        requestCreateTask.setName(TASK1_NAME);
        requestCreateTask.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreateTask);

        requestCreateTask.setName(TASK2_NAME);
        requestCreateTask.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreateTask).getTask().getId();

        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(userToken);
        requestCreateProject.setName(PROJECT1_NAME);
        requestCreateProject.setDescription(PROJECT1_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreateProject).getProject().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setProjectId(projectId);
        requestBind.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(2, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(1).getName());
        Assert.assertEquals(projectId, taskList.get(1).getProjectId());

        @NotNull final TaskBindToProjectRequest requestException = new TaskBindToProjectRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.bindTaskToProject(requestException);
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final TaskCreateRequest requestCreateTask = new TaskCreateRequest(userToken);
        requestCreateTask.setName(TASK1_NAME);
        requestCreateTask.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreateTask);

        requestCreateTask.setName(TASK2_NAME);
        requestCreateTask.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreateTask).getTask().getId();

        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(userToken);
        requestCreateProject.setName(PROJECT1_NAME);
        requestCreateProject.setDescription(PROJECT1_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreateProject).getProject().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setProjectId(projectId);
        requestBind.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskShowByIdRequest requestShow = new TaskShowByIdRequest(userToken);
        requestShow.setTaskId(taskId);
        @NotNull Task task = taskEndpoint.showByIdTask(requestShow).getTask();
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(projectId, task.getProjectId());

        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(userToken);
        requestUnbind.setProjectId(projectId);
        requestUnbind.setTaskId(taskId);
        taskEndpoint.unbindTaskFromProject(requestUnbind);

        task = taskEndpoint.showByIdTask(requestShow).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK2_NAME, task.getName());
        Assert.assertEquals(null, task.getProjectId());

        @NotNull final TaskUnbindFromProjectRequest requestException = new TaskUnbindFromProjectRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.unbindTaskFromProject(requestException);
    }

    @Test
    public void testListTaskByProjectId() {
        @NotNull final TaskCreateRequest requestCreateTask = new TaskCreateRequest(userToken);
        requestCreateTask.setName(TASK1_NAME);
        requestCreateTask.setDescription(TASK1_DESC);
        taskEndpoint.createTask(requestCreateTask);

        requestCreateTask.setName(TASK2_NAME);
        requestCreateTask.setDescription(TASK2_DESC);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreateTask).getTask().getId();

        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(userToken);
        requestCreateProject.setName(PROJECT1_NAME);
        requestCreateProject.setDescription(PROJECT1_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreateProject).getProject().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setProjectId(projectId);
        requestBind.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskListByProjectIdRequest requestList = new TaskListByProjectIdRequest(userToken);
        requestList.setProjectId(projectId);
        @NotNull TaskListByProjectIdResponse responseList = taskEndpoint.listTaskByProjectId(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(1, taskList.size());
        Assert.assertEquals(TASK2_NAME, taskList.get(0).getName());
        Assert.assertEquals(projectId, taskList.get(0).getProjectId());

        @NotNull final TaskListByProjectIdRequest requestException = new TaskListByProjectIdRequest(userToken);
        thrown.expect(Exception.class);
        taskEndpoint.listTaskByProjectId(requestException);
    }

}
