package ru.t1.vsukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.dto.request.project.*;
import ru.t1.sukhorukova.tm.dto.request.user.UserLoginRequest;
import ru.t1.sukhorukova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sukhorukova.tm.dto.response.project.*;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.service.PropertyService;
import ru.t1.vsukhorukova.tm.marker.IntegrationCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.USER_PASSWORD;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String getToken(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest(null);
        request.setLogin(login);
        request.setPassword(password);

        @Nullable final String token = authEndpoint.login(request).getToken();
        return token;
    }

    @Before
    public void login() {
        adminToken = getToken(ADMIN_LOGIN, ADMIN_PASSWORD);
        userToken = getToken(USER_LOGIN, USER_PASSWORD);
        projectEndpoint.clearProject(new ProjectClearRequest(adminToken));
        projectEndpoint.clearProject(new ProjectClearRequest(userToken));
    }

    @After
    public void logout() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        userToken = null;
        adminToken = null;
    }

    @Test
    public void testCreateProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);

        @NotNull final ProjectCreateResponse responseCreate = projectEndpoint.createProject(requestCreate);
        @Nullable Project projectCreate = responseCreate.getProject();
        Assert.assertNotNull(projectCreate);
        Assert.assertEquals(PROJECT1_NAME, projectCreate.getName());
        Assert.assertEquals(PROJECT1_DESC, projectCreate.getDescription());

        @NotNull final ProjectListRequest requestListUser = new ProjectListRequest(userToken);
        @NotNull final ProjectListResponse responseListUser = projectEndpoint.listProject(requestListUser);
        @Nullable final List<Project> projectListUser = responseListUser.getProjects();
        Assert.assertNotNull(projectListUser);
        Assert.assertEquals(1, projectListUser.size());
        Assert.assertEquals(PROJECT1_NAME, projectListUser.get(0).getName());
        Assert.assertEquals(PROJECT1_DESC, projectListUser.get(0).getDescription());

        @NotNull final ProjectListRequest requestListAdmin = new ProjectListRequest(adminToken);
        @NotNull final ProjectListResponse responseListAdmin = projectEndpoint.listProject(requestListAdmin);
        @Nullable final List<Project> projectListAdmin = responseListAdmin.getProjects();
        Assert.assertNull(projectListAdmin);

        @NotNull final ProjectCreateRequest requestException = new ProjectCreateRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.createProject(requestException);
    }

    @Test
    public void testListProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.createProject(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals(PROJECT1_NAME, projectList.get(0).getName());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT3_NAME, projectList.get(2).getName());
        Assert.assertEquals(PROJECT1_DESC, projectList.get(0).getDescription());
        Assert.assertEquals(PROJECT2_DESC, projectList.get(1).getDescription());
        Assert.assertEquals(PROJECT3_DESC, projectList.get(2).getDescription());
    }

    @Test
    public void testShowByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreate).getProject().getId();

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.createProject(requestCreate);

        @NotNull final ProjectShowByIdRequest requestShow = new ProjectShowByIdRequest(userToken);
        requestShow.setProjectId(projectId);
        @NotNull final ProjectShowByIdResponse responseShow = projectEndpoint.showByIdProject(requestShow);
        @Nullable final Project project = responseShow.getProject();
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(PROJECT2_DESC, project.getDescription());

        @NotNull final ProjectShowByIdRequest requestException = new ProjectShowByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.showByIdProject(requestException);
    }

    @Test
    public void testShowByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.createProject(requestCreate);

        @NotNull final ProjectShowByIndexRequest requestShow = new ProjectShowByIndexRequest(userToken);
        requestShow.setIndex(1);
        @NotNull final ProjectShowByIndexResponse responseShow = projectEndpoint.showByIndexProject(requestShow);
        @Nullable final Project project = responseShow.getProject();
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(PROJECT2_DESC, project.getDescription());

        @NotNull final ProjectShowByIndexRequest requestException = new ProjectShowByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.showByIndexProject(requestException);
    }

    @Test
    public void testClearProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.createProject(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());

        @NotNull final ProjectClearRequest requestClear = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClear);

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNull(projectList);
    }

    @Test
    public void testRemoveByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.createProject(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        @NotNull final String projectId = projectList.get(1).getId();

        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(projectId);
        projectEndpoint.removeByIdProject(requestRemove);

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertEquals(2, projectList.size());
        Assert.assertNotEquals(PROJECT2_NAME, projectList.get(1).getName());

        @NotNull final ProjectRemoveByIdRequest requestException = new ProjectRemoveByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.removeByIdProject(requestException);
    }

    @Test
    public void testRemoveByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.createProject(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        @NotNull final String projectId = projectList.get(1).getId();

        @NotNull final ProjectRemoveByIndexRequest requestRemove = new ProjectRemoveByIndexRequest(userToken);
        requestRemove.setIndex(1);
        projectEndpoint.removeByIndexProject(requestRemove);

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertEquals(2, projectList.size());
        Assert.assertNotEquals(PROJECT2_NAME, projectList.get(1).getName());

        @NotNull final ProjectRemoveByIndexRequest requestException = new ProjectRemoveByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.removeByIndexProject(requestException);
    }

    @Test
    public void testUpdateByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT2_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIdRequest requestUpdate = new ProjectUpdateByIdRequest(userToken);
        requestUpdate.setProjectId(projectId);
        requestUpdate.setName(PROJECT3_NAME);
        requestUpdate.setDescription(PROJECT3_DESC);
        @Nullable Project project = projectEndpoint.updateByIdProject(requestUpdate).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT3_NAME, project.getName());
        Assert.assertEquals(PROJECT3_DESC, project.getDescription());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT3_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT3_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIdRequest requestException = new ProjectUpdateByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.updateByIdProject(requestException);
    }

    @Test
    public void testUpdateByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT2_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIndexRequest requestUpdate = new ProjectUpdateByIndexRequest(userToken);
        requestUpdate.setIndex(1);
        requestUpdate.setName(PROJECT3_NAME);
        requestUpdate.setDescription(PROJECT3_DESC);
        @Nullable Project project = projectEndpoint.updateByIndexProject(requestUpdate).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT3_NAME, project.getName());
        Assert.assertEquals(PROJECT3_DESC, project.getDescription());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT3_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT3_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIndexRequest requestException = new ProjectUpdateByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.updateByIndexProject(requestException);
    }

    @Test
    public void testChangeStatusByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIdRequest requestChangeStatus = new ProjectChangeStatusByIdRequest(userToken);
        requestChangeStatus.setProjectId(projectId);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable Project project = projectEndpoint.changeStatusByIdProject(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIdRequest requestException = new ProjectChangeStatusByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.changeStatusByIdProject(requestException);
    }

    @Test
    public void testChangeStatusByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIndexRequest requestChangeStatus = new ProjectChangeStatusByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable Project project = projectEndpoint.changeStatusByIndexProject(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIndexRequest requestException = new ProjectChangeStatusByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.changeStatusByIndexProject(requestException);
    }

    @Test
    public void testCompleteByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIdRequest requestChangeStatus = new ProjectCompleteByIdRequest(userToken);
        requestChangeStatus.setProjectId(projectId);
        @Nullable Project project = projectEndpoint.completeByIdProject(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIdRequest requestException = new ProjectCompleteByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.completeByIdProject(requestException);
    }

    @Test
    public void testCompleteByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIndexRequest requestChangeStatus = new ProjectCompleteByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable Project project = projectEndpoint.completeByIndexProject(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIndexRequest requestException = new ProjectCompleteByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.completeByIndexProject(requestException);
    }

    @Test
    public void testStartByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIdRequest requestChangeStatus = new ProjectStartByIdRequest(userToken);
        requestChangeStatus.setProjectId(projectId);
        @Nullable Project project = projectEndpoint.startByIdProject(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIdRequest requestException = new ProjectStartByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.startByIdProject(requestException);
    }

    @Test
    public void testStartByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.createProject(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.createProject(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.listProject(requestList);
        @Nullable List<Project> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIndexRequest requestChangeStatus = new ProjectStartByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable Project project = projectEndpoint.startByIndexProject(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.listProject(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIndexRequest requestException = new ProjectStartByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.startByIndexProject(requestException);
    }

}
