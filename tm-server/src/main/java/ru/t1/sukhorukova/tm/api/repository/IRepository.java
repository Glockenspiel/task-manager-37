package ru.t1.sukhorukova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    @NotNull
    M removeOne(@NotNull M model);

    @Nullable
    M removeOneById(@NotNull String id);

    void removeAll();

    int getSize();

    boolean existsById(@NotNull String id);

}
