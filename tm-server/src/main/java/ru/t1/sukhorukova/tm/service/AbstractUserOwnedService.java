package ru.t1.sukhorukova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.IRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserOwnerRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.IUserOwnedService;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IndexIncorrectException;
import ru.t1.sukhorukova.tm.exception.field.UserIdEmptyException;
import ru.t1.sukhorukova.tm.model.AbstractModel;
import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnerModel, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnerRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @Override
    @SneakyThrows
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            repository.add(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            repository.removeAll(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            return (M)repository.findOneById(userId, id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @NotNull final List<M> models = findAll(userId);
        return models.get(index);
    }

    @Override
    @SneakyThrows
    public void removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            repository.removeOne(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final AbstractModel result;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            result = repository.removeOneById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return (M)result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<M> models = findAll(userId);
        return removeOneById(models.get(index).getId());
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnerRepository repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

}
