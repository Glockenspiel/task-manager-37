package ru.t1.sukhorukova.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @Getter
    @NotNull
    private final String tableName = "TM_PROJECT";

    public ProjectRepository(final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("ID"));
        project.setName(row.getString("NAME"));
        project.setDescription(row.getString("DESCRIPTION"));
        project.setUserId(row.getString("USER_ID"));
        project.setStatus(Status.toStatus(row.getString("STATUS")));
        project.setCreated(row.getTimestamp("CREATED"));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, NAME, DESCRIPTION, USER_ID, STATUS, CREATED) " +
                    "VALUES (?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getUserId());
            statement.setString(5, project.getStatus().name());
            statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @NotNull final String userId,
            @NotNull final ProjectSort sort
    ) {
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE USER_ID = ? ORDER BY %s",
                getTableName(),
                sort.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void updateById(
            @NotNull final String userId,
            @NotNull final Project project
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s" +
                "   SET NAME = ?," +
                "       DESCRIPTION = ?" +
                " WHERE ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getId());
            statement.setString(4, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void changeProjectStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET STATUS = ? WHERE ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, status.name());
            statement.setString(2, id);
            statement.setString(3, userId);
            statement.executeUpdate();
        }
    }

}
