package ru.t1.sukhorukova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

    void setPasswordHashById(final String id, String passwordHash);

    void update(@NotNull final User user);

    void setLockById(
            @NotNull final String id,
            @NotNull final Boolean lock
    );

}
