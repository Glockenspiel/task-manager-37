package ru.t1.sukhorukova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.AbstractModel;
import ru.t1.sukhorukova.tm.api.repository.IRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    protected abstract String getTableName();

    @NotNull
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        for (final M model : models) {
            add(model);
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        removeAll();
        return add(models);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE ID = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeOne(@NotNull final M model) {
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        return removeOne(findOneById(id));
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final String sql = String.format(
                "DELETE FROM %s",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String sql = String.format(
                "SELECT COUNT(1) AS COUNT FROM %s",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            rowSet.next();
            return rowSet.getInt("COUNT");
        }
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

}
