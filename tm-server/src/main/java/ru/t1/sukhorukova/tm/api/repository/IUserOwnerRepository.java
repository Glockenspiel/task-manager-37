package ru.t1.sukhorukova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    @Nullable
    M add(
            @Nullable String userId,
            @NotNull M model
    );

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M removeOne(
            @Nullable String userId,
            @Nullable M model
    );

    @Nullable
    M removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void removeAll(@NotNull String userId);

    int getSize(@NotNull String userId);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

}
