package ru.t1.sukhorukova.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Getter
    @NotNull
    private final String tableName = "TM_TASK";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("ID"));
        task.setName(row.getString("NAME"));
        task.setDescription(row.getString("DESCRIPTION"));
        task.setUserId(row.getString("USER_ID"));
        task.setProjectId(row.getString("PROJECT_ID"));
        task.setStatus(Status.toStatus(row.getString("STATUS")));
        task.setCreated(row.getTimestamp("CREATED"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, NAME, DESCRIPTION, USER_ID, PROJECT_ID, STATUS, CREATED) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setString(3, task.getDescription());
            statement.setString(4, task.getUserId());
            statement.setString(5, task.getProjectId());
            statement.setString(6, task.getStatus().name());
            statement.setTimestamp(7, new Timestamp(task.getCreated().getTime()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(
            @NotNull final String userId,
            @NotNull final TaskSort sort
    ) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE USER_ID = ? ORDER BY %s",
                getTableName(),
                sort.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE PROJECT_ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void updateById(
            @NotNull final String userId,
            @NotNull final Task task
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s" +
                "   SET NAME = ?," +
                "       DESCRIPTION = ?" +
                " WHERE ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getId());
            statement.setString(4, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET STATUS = ? WHERE ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, status.name());
            statement.setString(2, id);
            statement.setString(3, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET PROJECT_ID = ? WHERE ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, taskId);
            statement.setString(3, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET PROJECT_ID = NULL WHERE ID = ? AND PROJECT_ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, taskId);
            statement.setString(2, projectId);
            statement.setString(3, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void removeTasksByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId
    ) {
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE PROJECT_ID = ? AND USER_ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            statement.executeUpdate();
        }
    }

}
