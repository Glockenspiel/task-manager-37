package ru.t1.sukhorukova.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Getter
    @NotNull
    private final String tableName = "TM_USER";

    public UserRepository(final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("ID"));
        user.setLogin(row.getString("LOGIN"));
        user.setPasswordHash(row.getString("PASSWORD_HASH"));
        user.setRole(Role.toRole(row.getString("ROLE")));
        user.setLocked(row.getBoolean("LOCKED"));
        user.setEmail(row.getString("EMAIL"));
        user.setFirstName(row.getString("FIRST_NAME"));
        user.setLastName(row.getString("LAST_NAME"));
        user.setMiddleName(row.getString("MIDDLE_NAME"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, LOGIN, PASSWORD_HASH, ROLE, LOCKED, EMAIL, FIRST_NAME, LAST_NAME, MIDDLE_NAME) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getRole().name());
            statement.setBoolean(5, user.getLocked());
            statement.setString(6, user.getEmail());
            statement.setString(7, user.getFirstName());
            statement.setString(8, user.getLastName());
            statement.setString(9, user.getMiddleName());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE LOGIN = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE EMAIL = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void setPasswordHashById(
            @NotNull final String id,
            @NotNull final String passwordHash
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET PASSWORD_HASH = ? WHERE ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, passwordHash);
            statement.setString(2, id);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void update(
            @NotNull final User user
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s" +
                "   SET FIRST_NAME = ?," +
                "       LAST_NAME = ?," +
                "       MIDDLE_NAME = ?" +
                " WHERE ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getMiddleName());
            statement.setString(4, user.getId());
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public void setLockById(
            @NotNull final String id,
            @NotNull final Boolean lock
    ) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET LOCKED = ? WHERE ID = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, lock);
            statement.setString(2, id);
            statement.executeUpdate();
        }
    }

}
