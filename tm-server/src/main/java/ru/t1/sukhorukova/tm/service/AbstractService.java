package ru.t1.sukhorukova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.IRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IndexIncorrectException;
import ru.t1.sukhorukova.tm.model.AbstractModel;
import ru.t1.sukhorukova.tm.api.service.IService;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository repository = getRepository(connection);
            repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @SneakyThrows
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null) throw new ValueIsNullException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository repository = getRepository(connection);
            repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @SneakyThrows
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null) throw new ValueIsNullException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository repository = getRepository(connection);
            repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository repository = getRepository(connection);
            repository.removeAll();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository repository = getRepository(connection);
            return (M)repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<M> models = findAll();
        return models.get(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository repository = getRepository(connection);
            repository.removeOne(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final AbstractModel result;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository repository = getRepository(connection);
            result = repository.removeOneById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return (M)result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<M> models = findAll();
        return removeOneById(models.get(index).getId());
    }

    @Override
    @SneakyThrows
    public int getSize() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

}
