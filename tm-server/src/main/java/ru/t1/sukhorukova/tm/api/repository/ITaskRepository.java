package ru.t1.sukhorukova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @NotNull
    List<Task> findAll(
            @NotNull String userId,
            @NotNull TaskSort sort
    );

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void updateById(
            @NotNull final String userId,
            @NotNull final Task task
    );

    void changeTaskStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    );

    void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String projectId
    );

    void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String projectId
    );

    void removeTasksByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId
    );

}
