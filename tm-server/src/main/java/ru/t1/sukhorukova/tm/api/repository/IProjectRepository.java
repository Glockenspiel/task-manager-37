package ru.t1.sukhorukova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    @NotNull
    List<Project> findAll(
            @NotNull String userId,
            @NotNull ProjectSort sort
    );

    void updateById(
            @NotNull final String userId,
            @NotNull final Project project
    );

    void changeProjectStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    );

}
