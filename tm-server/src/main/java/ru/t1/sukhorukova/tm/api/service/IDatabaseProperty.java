package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseLogin();

    @NotNull
    public String getDatabasePassword();

    @NotNull
    public String getDatabaseUrl();

}
