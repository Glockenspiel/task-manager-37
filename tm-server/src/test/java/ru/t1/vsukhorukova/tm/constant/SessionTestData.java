package ru.t1.vsukhorukova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.Session;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.ADMIN;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static Session USER1_SESSION1 = new Session(USER1);

    @NotNull
    public final static Session USER1_SESSION2 = new Session(USER1);

    @NotNull
    public final static Session USER1_SESSION3 = new Session(USER1);

    @NotNull
    public final static Session USER2_SESSION1 = new Session(USER2);

    @NotNull
    public final static Session ADMIN_SESSION1 = new Session(ADMIN);

    @NotNull
    public final static Session ADMIN_SESSION2 = new Session(ADMIN);

    @NotNull
    public final static List<Session> USER1_SESSION_LIST = Arrays.asList(USER1_SESSION1, USER1_SESSION2, USER1_SESSION3);

    @NotNull
    public final static List<Session> USER2_SESSION_LIST = Collections.singletonList(USER2_SESSION1);

    @NotNull
    public final static List<Session> ADMIN_SESSION_LIST = Arrays.asList(ADMIN_SESSION1, ADMIN_SESSION2);

    @Nullable
    public final static Session NULL_SESSION = null;

    @Nullable
    public final static String NULL_SESSION_ID = null;

    @Nullable
    public final static List<Session> NULL_SESSION_LIST = null;

}
