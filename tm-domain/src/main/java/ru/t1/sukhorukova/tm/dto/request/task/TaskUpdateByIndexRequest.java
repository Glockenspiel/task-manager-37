package ru.t1.sukhorukova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;
    
    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIndexRequest(@Nullable String token) {
        super(token);
    }

}
