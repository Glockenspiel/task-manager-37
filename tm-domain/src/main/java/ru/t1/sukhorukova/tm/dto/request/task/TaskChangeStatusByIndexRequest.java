package ru.t1.sukhorukova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.AbstractUserRequest;
import ru.t1.sukhorukova.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(@Nullable String token) {
        super(token);
    }

}
